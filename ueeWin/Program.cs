﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ueeWin
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmLogin T = new frmLogin();
            T.ShowDialog();

            if (T.OK)
            {
                
                Application.Run(new frmMain());
            }
        }
    }
}
