﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            txtNroEquip.Text = CParam.getParametro("idEquipamento").Trim();
        }

        private void txtNroEquip_Enter(object sender, EventArgs e)
        {
            txtNroEquip.SelectAll();
        }

        private void cmdCanc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dadosValidos()
        {
            if(txtNroEquip.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe o número do equipamento.");
                txtNroEquip.Focus();
                return false;
            }

            return true;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (!dadosValidos()) return;

            CParam.setParametro("idEquipamento", txtNroEquip.Text.Trim());

            CGeral.MsgErro("Configurações aplicadas");

            this.Close();
        }


    }
}
