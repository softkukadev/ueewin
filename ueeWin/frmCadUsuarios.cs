using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmCadUsuarios : Form 
    {
        //Par�metros de configura��o da janela
        public int TipoOperacao;
        public frmLocUsuarios Chamador;
        public int ID;
        public DataGridViewRow Linha;

        public frmCadUsuarios()
        {
            InitializeComponent();
        }

        private void form_Load(object sender, EventArgs e)
        {
            if (this.TipoOperacao == 1)
            {
                this.Text = "Novo usu�rio";
            }
            else
            {
                CUser xUser = CUser.getById(this.ID);
                this.Text = "Alterando usu�rio " +xUser.login;
                
                lblID.Text = xUser.id.ToString();
                txtNome.Text = xUser.login;
                txtSenha.Text = ""; txtSenha.Enabled = false;
                chkAdmin.Checked = xUser.isAdmin;
                imgInativo.Visible = !xUser.ativo;
            }

        }
        
        private void form_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) SendKeys.Send("{TAB}");
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool DadosValidos()
        {

            if (txtNome.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe o login.");
                txtNome.Focus();
                return false;
            }

            if (this.TipoOperacao == 1)
            {
                if (txtSenha.Text.Trim() == "")
                {
                    CGeral.MsgErro("Informe a senha.");
                    txtSenha.Focus();
                    return false;
                }
            }
            return true;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            CUser xUser = new CUser();

            //Valida��es
            if (this.TipoOperacao != 3)
            {

                if (!DadosValidos())
                    return;

                //Grava
                xUser.id = this.TipoOperacao == 1 ? -1 : this.ID;
                xUser.login = txtNome.Text.Trim();
                xUser.ssd = this.TipoOperacao == 1 ? txtSenha.Text.Trim() : "";
                xUser.isAdmin = chkAdmin.Checked;

                CUser.save(xUser);
            }
            else
                CUser.delete(this.ID);

            //Atualiza
            if (this.Chamador != null)
            {

                if (this.TipoOperacao == 3)
                    this.Chamador.cmdLoc_Click(sender, e);
                else
                {
                    if (this.Linha != null)
                    {
                        this.Linha.Cells[1].Value = xUser.login;
                        this.Linha.Cells[2].Value = xUser.isAdmin;
                    }
                    else
                        this.Chamador.cmdLoc_Click(sender, e);
                }
            }

            this.Close();
 
        }
    }
}
