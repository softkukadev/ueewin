using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmLocUsuarios : Form
    {

        //Propriedades de Configura��o
        public int TipoTela;    //1 Cadastro 2 - Pesquisa 3 - Adicionar Pessoa
        public Form Chamador;
        public object ChamadorCod;
        public object ChamadorDescr;

        public delegate void RetornouHandler(string Codigo, string Descricao);
        public event RetornouHandler Retornou;

        //Inicializa��o da Tela
        public frmLocUsuarios()
        {
            InitializeComponent();
            G.AutoGenerateColumns = false;
        }

        //Eventos
        private void frmLoc_Load(object sender, EventArgs e)
        {
            if (this.TipoTela == 1)
                this.Text = "Cadastro de Usu�rios";
            else
                this.Text = "Pesquisa de Usu�rios";

            cmbID.SelectedIndex = 0;
            cmbNome.SelectedIndex = 1;
            
        }

        private void form_Activated(object sender, EventArgs e)
        {
            txtNome.Focus();
        }

        private void form_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) SendKeys.Send("{TAB}");
        }


        public void cmdLoc_Click(object sender, EventArgs e)
        {
            Localizar();
        }

        private void mnuNovo_Click(object sender, EventArgs e)
        {
            Inserir();
        }

        private void mnuAlterar_Click(object sender, EventArgs e)
        {
            Alterar();
        }

        private void G_DoubleClick(object sender, EventArgs e)
        {
            if (this.TipoTela == 1)
                Alterar();

            if (this.TipoTela == 2)
                Retornar();
        }

        private void mnuDesativar_Click(object sender, EventArgs e)
        {
            Desativar();
        }

        private void mnuAtiva_Click(object sender, EventArgs e)
        {
            Ativar();
        }

        private void mnuTrocarSenha_Click(object sender, EventArgs e)
        {
            TrocarSenha();
        }

        private void txtID_Enter(object sender, EventArgs e)
        {
            txtID.SelectAll();
            txtID.SelectionStart = 0;
        }

        private void txtID_Click(object sender, EventArgs e)
        {
            txtID.SelectAll();
            txtID.SelectionStart = 0;
        }


        private void Localizar()
        {
            //Validar os dados
            if (cmbID.Text != "<Todos>" && txtID.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe o c�digo para pesquisa!");
                txtID.Focus();
                return;
            }

            if ((cmbNome.Text != "<Todos>" && cmbNome.Text != "Cont�m") && txtNome.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe o nome para pesquisa!");
                txtNome.Focus();
                return;

            }

            //Monta os filtros
            string critID = "all"; string valID = "0";
            switch (cmbID.Text)
            {
                case "Igual":
                    critID = "="; valID = txtID.Text.Trim(); break;
                case "Maior":
                    critID = ">"; valID = txtID.Text.Trim(); break;
                case "Menor":
                    critID = "<"; valID = txtID.Text.Trim(); break;
            }

            string critNome = "all"; string valNome = "";
            switch (cmbNome.Text)
            {
                case "Igual":
                    critNome = "="; valNome = txtNome.Text.Trim(); break;
                case "Cont�m":
                    critNome = "ct"; valNome = txtNome.Text.Trim(); break;
            }

            string Estado = "";
            if (optAtivos.Checked) Estado = "A";
            if (optInativos.Checked) Estado = "I";

            DataSet DS = CUser.getAll(critID, valID, critNome, valNome, Estado);
            pnlReg.Text = DS.Tables[0].Rows.Count.ToString() + " #";

            G.DataSource = DS.Tables[0];
            //Marcar os inativos
            foreach (DataGridViewRow L in G.Rows)
            {
                bool Ativo = (bool)L.Cells[3].Value;
                if (!Ativo)
                    L.DefaultCellStyle.ForeColor = Color.Red;
            }

            pnlReg.Text = G.RowCount + "#";
        }

        private void Inserir()
        {
            frmCadUsuarios T = new frmCadUsuarios();
            T.MdiParent = this.MdiParent;
            T.TipoOperacao = 1;
            T.Chamador = this;
            T.Show();
        }

        private void Alterar()
        {
            if (G.SelectedRows.Count > 0)
            {
                //Obtem Seq
                int Seq = (int)G.SelectedRows[0].Cells[0].Value;

                frmCadUsuarios T = new frmCadUsuarios();
                T.ID = Seq;
                T.MdiParent = this.MdiParent;
                T.TipoOperacao = 2;
                T.Chamador = this;
                T.Linha = G.SelectedRows[0];
                T.Show();
            }
        }

        private void Excluir()
        {
            if (G.SelectedRows.Count > 0)
            {
                //Obtem Seq
                int Seq = (int)G.SelectedRows[0].Cells[0].Value;

                frmCadUsuarios T = new frmCadUsuarios();
                T.ID = Seq;
                T.TipoOperacao = 3;
                T.Chamador = this;
                T.Linha = G.SelectedRows[0];
                T.Show();
            }

        }

        private void Retornar()
        {
            if (G.SelectedRows.Count > 0)
            {

                if (this.ChamadorCod.GetType() == typeof(MaskedTextBox))
                {
                    ((MaskedTextBox)this.ChamadorCod).Text = G.SelectedRows[0].Cells[0].Value.ToString();
                }
                else if (this.ChamadorCod.GetType() == typeof(TextBox))
                {
                    ((TextBox)this.ChamadorCod).Text = G.SelectedRows[0].Cells[0].Value.ToString();
                }

                if (this.ChamadorDescr.GetType() == typeof(TextBox))
                {
                    ((TextBox)this.ChamadorDescr).Text = G.SelectedRows[0].Cells[1].Value.ToString();
                }
                else if (this.ChamadorDescr.GetType() == typeof(Label)) 
                {
                    ((Label)this.ChamadorDescr).Text = G.SelectedRows[0].Cells[1].Value.ToString();
                }

                    
                if (this.Retornou != null)
                    Retornou(G.SelectedRows[0].Cells[0].Value.ToString(), G.SelectedRows[0].Cells[1].Value.ToString());


                this.Close();
            }
        }

        private void Ativar()
        {
            if (G.SelectedRows.Count > 0)
            {
                bool Ativo = (bool)G.SelectedRows[0].Cells[3].Value;
                if (Ativo)
                {
                    CGeral.MsgErro("Este registro j� esta ativado.");
                    return;
                }

                int id = (int)G.SelectedRows[0].Cells[0].Value;
                CUser.activate(id);

                cmdLoc_Click(this, new EventArgs());
            }
        }

        private void Desativar()
        {
            if (G.SelectedRows.Count > 0)
            {
                bool Ativo = (bool)G.SelectedRows[0].Cells[3].Value;
                if (!Ativo)
                {
                    CGeral.MsgErro("Este registro j� esta desativado.");
                    return;
                }

                int id = (int)G.SelectedRows[0].Cells[0].Value;
                CUser.deactivate(id);

                cmdLoc_Click(this, new EventArgs());
            }
        }

        private void TrocarSenha()
        {
            if (G.SelectedRows.Count > 0)
            {
                int id = (int)G.SelectedRows[0].Cells[0].Value;
                frmCadUsuarioSenha T = new frmCadUsuarioSenha();
                T.ID = id;
                T.ShowDialog();
            }
        }

    }
}