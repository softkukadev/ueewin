namespace ueeWin
{
    partial class frmLocUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLocUsuarios));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbID = new System.Windows.Forms.ComboBox();
            this.cmbNome = new System.Windows.Forms.ComboBox();
            this.txtID = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.cmdLoc = new System.Windows.Forms.Button();
            this.T = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.pgbLoc = new System.Windows.Forms.ToolStripProgressBar();
            this.pnlReg = new System.Windows.Forms.ToolStripStatusLabel();
            this.ctxCad = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuNovo = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAlterar = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDesativar = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAtiva = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTrocarSenha = new System.Windows.Forms.ToolStripMenuItem();
            this.G = new System.Windows.Forms.DataGridView();
            this.clID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clIEs = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clAtivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.optTodos = new System.Windows.Forms.RadioButton();
            this.optAtivos = new System.Windows.Forms.RadioButton();
            this.optInativos = new System.Windows.Forms.RadioButton();
            this.statusStrip1.SuspendLayout();
            this.ctxCad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.G)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "C�digo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nome:";
            // 
            // cmbID
            // 
            this.cmbID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbID.FormattingEnabled = true;
            this.cmbID.Items.AddRange(new object[] {
            "<Todos>",
            "Igual",
            "Maior",
            "Menor"});
            this.cmbID.Location = new System.Drawing.Point(61, 6);
            this.cmbID.Name = "cmbID";
            this.cmbID.Size = new System.Drawing.Size(73, 21);
            this.cmbID.TabIndex = 5;
            this.cmbID.TabStop = false;
            // 
            // cmbNome
            // 
            this.cmbNome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNome.FormattingEnabled = true;
            this.cmbNome.Items.AddRange(new object[] {
            "<Todos>",
            "Cont�m",
            "Igual"});
            this.cmbNome.Location = new System.Drawing.Point(61, 32);
            this.cmbNome.Name = "cmbNome";
            this.cmbNome.Size = new System.Drawing.Size(73, 21);
            this.cmbNome.TabIndex = 1;
            this.cmbNome.TabStop = false;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(140, 6);
            this.txtID.Mask = "00000";
            this.txtID.Name = "txtID";
            this.txtID.PromptChar = ' ';
            this.txtID.Size = new System.Drawing.Size(47, 20);
            this.txtID.TabIndex = 6;
            this.txtID.ValidatingType = typeof(int);
            this.txtID.Click += new System.EventHandler(this.txtID_Click);
            this.txtID.Enter += new System.EventHandler(this.txtID_Enter);
            // 
            // txtNome
            // 
            this.txtNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNome.Location = new System.Drawing.Point(140, 32);
            this.txtNome.MaxLength = 40;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(263, 20);
            this.txtNome.TabIndex = 2;
            // 
            // cmdLoc
            // 
            this.cmdLoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdLoc.Image = ((System.Drawing.Image)(resources.GetObject("cmdLoc.Image")));
            this.cmdLoc.Location = new System.Drawing.Point(409, 6);
            this.cmdLoc.Name = "cmdLoc";
            this.cmdLoc.Size = new System.Drawing.Size(59, 46);
            this.cmdLoc.TabIndex = 3;
            this.T.SetToolTip(this.cmdLoc, "Localizar");
            this.cmdLoc.UseVisualStyleBackColor = true;
            this.cmdLoc.Click += new System.EventHandler(this.cmdLoc_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pgbLoc,
            this.pnlReg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 266);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(475, 24);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // pgbLoc
            // 
            this.pgbLoc.Name = "pgbLoc";
            this.pgbLoc.Size = new System.Drawing.Size(100, 18);
            // 
            // pnlReg
            // 
            this.pnlReg.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.pnlReg.Name = "pnlReg";
            this.pnlReg.Size = new System.Drawing.Size(24, 19);
            this.pnlReg.Text = "0#";
            this.pnlReg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ctxCad
            // 
            this.ctxCad.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNovo,
            this.mnuAlterar,
            this.mnuDesativar,
            this.mnuAtiva,
            this.mnuTrocarSenha});
            this.ctxCad.Name = "ctxCad";
            this.ctxCad.Size = new System.Drawing.Size(181, 136);
            // 
            // mnuNovo
            // 
            this.mnuNovo.Name = "mnuNovo";
            this.mnuNovo.Size = new System.Drawing.Size(180, 22);
            this.mnuNovo.Text = "&Novo...";
            this.mnuNovo.Click += new System.EventHandler(this.mnuNovo_Click);
            // 
            // mnuAlterar
            // 
            this.mnuAlterar.Name = "mnuAlterar";
            this.mnuAlterar.Size = new System.Drawing.Size(180, 22);
            this.mnuAlterar.Text = "&Alterar...";
            this.mnuAlterar.Click += new System.EventHandler(this.mnuAlterar_Click);
            // 
            // mnuDesativar
            // 
            this.mnuDesativar.Name = "mnuDesativar";
            this.mnuDesativar.Size = new System.Drawing.Size(180, 22);
            this.mnuDesativar.Text = "&Desativar";
            this.mnuDesativar.Click += new System.EventHandler(this.mnuDesativar_Click);
            // 
            // mnuAtiva
            // 
            this.mnuAtiva.Name = "mnuAtiva";
            this.mnuAtiva.Size = new System.Drawing.Size(180, 22);
            this.mnuAtiva.Text = "A&tivar";
            this.mnuAtiva.Click += new System.EventHandler(this.mnuAtiva_Click);
            // 
            // mnuTrocarSenha
            // 
            this.mnuTrocarSenha.Name = "mnuTrocarSenha";
            this.mnuTrocarSenha.Size = new System.Drawing.Size(180, 22);
            this.mnuTrocarSenha.Text = "Trocar &Senha";
            this.mnuTrocarSenha.Click += new System.EventHandler(this.mnuTrocarSenha_Click);
            // 
            // G
            // 
            this.G.AllowUserToAddRows = false;
            this.G.AllowUserToDeleteRows = false;
            this.G.AllowUserToResizeRows = false;
            this.G.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.G.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.G.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clID,
            this.clNome,
            this.clIEs,
            this.clAtivo});
            this.G.ContextMenuStrip = this.ctxCad;
            this.G.Location = new System.Drawing.Point(8, 85);
            this.G.Name = "G";
            this.G.RowHeadersWidth = 21;
            this.G.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.G.Size = new System.Drawing.Size(460, 178);
            this.G.TabIndex = 10;
            this.G.DoubleClick += new System.EventHandler(this.G_DoubleClick);
            // 
            // clID
            // 
            this.clID.DataPropertyName = "id";
            this.clID.HeaderText = "ID";
            this.clID.Name = "clID";
            this.clID.ReadOnly = true;
            this.clID.Width = 60;
            // 
            // clNome
            // 
            this.clNome.DataPropertyName = "Login";
            this.clNome.HeaderText = "Login";
            this.clNome.Name = "clNome";
            this.clNome.ReadOnly = true;
            this.clNome.Width = 180;
            // 
            // clIEs
            // 
            this.clIEs.DataPropertyName = "isAdmin";
            this.clIEs.HeaderText = "Admin";
            this.clIEs.Name = "clIEs";
            this.clIEs.ReadOnly = true;
            this.clIEs.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clIEs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clAtivo
            // 
            this.clAtivo.DataPropertyName = "Ativo";
            this.clAtivo.HeaderText = "Ativo";
            this.clAtivo.Name = "clAtivo";
            this.clAtivo.ReadOnly = true;
            this.clAtivo.Visible = false;
            // 
            // optTodos
            // 
            this.optTodos.AutoSize = true;
            this.optTodos.Checked = true;
            this.optTodos.Location = new System.Drawing.Point(140, 58);
            this.optTodos.Name = "optTodos";
            this.optTodos.Size = new System.Drawing.Size(55, 17);
            this.optTodos.TabIndex = 7;
            this.optTodos.TabStop = true;
            this.optTodos.Text = "Todos";
            this.optTodos.UseVisualStyleBackColor = true;
            // 
            // optAtivos
            // 
            this.optAtivos.AutoSize = true;
            this.optAtivos.Location = new System.Drawing.Point(201, 58);
            this.optAtivos.Name = "optAtivos";
            this.optAtivos.Size = new System.Drawing.Size(54, 17);
            this.optAtivos.TabIndex = 8;
            this.optAtivos.Text = "Ativos";
            this.optAtivos.UseVisualStyleBackColor = true;
            // 
            // optInativos
            // 
            this.optInativos.AutoSize = true;
            this.optInativos.Location = new System.Drawing.Point(257, 58);
            this.optInativos.Name = "optInativos";
            this.optInativos.Size = new System.Drawing.Size(62, 17);
            this.optInativos.TabIndex = 9;
            this.optInativos.Text = "Inativos";
            this.optInativos.UseVisualStyleBackColor = true;
            // 
            // frmLocUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(475, 290);
            this.Controls.Add(this.optInativos);
            this.Controls.Add(this.optAtivos);
            this.Controls.Add(this.optTodos);
            this.Controls.Add(this.G);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.cmdLoc);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.cmbNome);
            this.Controls.Add(this.cmbID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "frmLocUsuarios";
            this.Text = "Cadastro de Usu�rios";
            this.Activated += new System.EventHandler(this.form_Activated);
            this.Load += new System.EventHandler(this.frmLoc_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.form_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ctxCad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.G)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbID;
        private System.Windows.Forms.ComboBox cmbNome;
        private System.Windows.Forms.MaskedTextBox txtID;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Button cmdLoc;
        private System.Windows.Forms.ToolTip T;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar pgbLoc;
        private System.Windows.Forms.ToolStripStatusLabel pnlReg;
        private System.Windows.Forms.ContextMenuStrip ctxCad;
        private System.Windows.Forms.ToolStripMenuItem mnuNovo;
        private System.Windows.Forms.ToolStripMenuItem mnuAlterar;
        private System.Windows.Forms.DataGridView G;
        private System.Windows.Forms.RadioButton optTodos;
        private System.Windows.Forms.RadioButton optAtivos;
        private System.Windows.Forms.RadioButton optInativos;
        private System.Windows.Forms.ToolStripMenuItem mnuDesativar;
        private System.Windows.Forms.ToolStripMenuItem mnuAtiva;
        private System.Windows.Forms.DataGridViewTextBoxColumn clID;
        private System.Windows.Forms.DataGridViewTextBoxColumn clNome;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clIEs;
        private System.Windows.Forms.DataGridViewTextBoxColumn clAtivo;
        private System.Windows.Forms.ToolStripMenuItem mnuTrocarSenha;
    }
}