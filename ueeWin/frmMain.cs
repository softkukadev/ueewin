﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //gravado usuario  root arroz
            this.Text = CConfig.NOME_APP + " - Versão " + CConfig.VERSAO + " de " + CConfig.VERSAO_DATA;

            this.staUser.Text = CLogin.userLogin;
            if (CLogin.isAdmin)
            {
                this.staUser.Text += "(admin)";
            }

        }

        private void mnuCadUser_Click(object sender, EventArgs e)
        {
            frmLocUsuarios T = new frmLocUsuarios();
            T.TipoTela = 1;
            T.MdiParent = this;
            T.Show();
        }

        private void mnuHelpConfig_Click(object sender, EventArgs e)
        {
            frmConfig T = new frmConfig();
            T.MdiParent = this;
            T.Show();
        }
    }
}
