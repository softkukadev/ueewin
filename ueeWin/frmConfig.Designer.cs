﻿namespace ueeWin
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNome = new System.Windows.Forms.Label();
            this.txtNroEquip = new System.Windows.Forms.MaskedTextBox();
            this.cmdCanc = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(12, 31);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(127, 13);
            this.lblNome.TabIndex = 7;
            this.lblNome.Text = "Número do Equipamento:";
            // 
            // txtNroEquip
            // 
            this.txtNroEquip.AsciiOnly = true;
            this.txtNroEquip.Location = new System.Drawing.Point(145, 24);
            this.txtNroEquip.Mask = "00000";
            this.txtNroEquip.Name = "txtNroEquip";
            this.txtNroEquip.PromptChar = ' ';
            this.txtNroEquip.Size = new System.Drawing.Size(72, 20);
            this.txtNroEquip.TabIndex = 8;
            this.txtNroEquip.Text = "0";
            this.txtNroEquip.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNroEquip.ValidatingType = typeof(int);
            this.txtNroEquip.Enter += new System.EventHandler(this.txtNroEquip_Enter);
            // 
            // cmdCanc
            // 
            this.cmdCanc.Image = global::ueeWin.Properties.Resources.Cancel;
            this.cmdCanc.Location = new System.Drawing.Point(101, 108);
            this.cmdCanc.Name = "cmdCanc";
            this.cmdCanc.Size = new System.Drawing.Size(55, 40);
            this.cmdCanc.TabIndex = 10;
            this.cmdCanc.UseVisualStyleBackColor = true;
            this.cmdCanc.Click += new System.EventHandler(this.cmdCanc_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Image = global::ueeWin.Properties.Resources.OK;
            this.cmdOK.Location = new System.Drawing.Point(162, 108);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(55, 40);
            this.cmdOK.TabIndex = 9;
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(250, 160);
            this.Controls.Add(this.cmdCanc);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.txtNroEquip);
            this.Controls.Add(this.lblNome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurações";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.MaskedTextBox txtNroEquip;
        private System.Windows.Forms.Button cmdCanc;
        private System.Windows.Forms.Button cmdOK;
    }
}