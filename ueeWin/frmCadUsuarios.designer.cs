namespace ueeWin
{
    partial class frmCadUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.T = new System.Windows.Forms.ToolTip(this.components);
            this.cmdCanc = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.imgInativo = new System.Windows.Forms.PictureBox();
            this.lblID = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.chkAdmin = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgInativo)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdCanc
            // 
            this.cmdCanc.Image = global::ueeWin.Properties.Resources.Cancel;
            this.cmdCanc.Location = new System.Drawing.Point(244, 118);
            this.cmdCanc.Name = "cmdCanc";
            this.cmdCanc.Size = new System.Drawing.Size(55, 40);
            this.cmdCanc.TabIndex = 4;
            this.T.SetToolTip(this.cmdCanc, "Cancelar");
            this.cmdCanc.UseVisualStyleBackColor = true;
            this.cmdCanc.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.Image = global::ueeWin.Properties.Resources.OK;
            this.cmdOK.Location = new System.Drawing.Point(305, 118);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(55, 40);
            this.cmdOK.TabIndex = 3;
            this.T.SetToolTip(this.cmdOK, "Gravar Registro");
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // imgInativo
            // 
            this.imgInativo.Image = global::ueeWin.Properties.Resources.Inativo;
            this.imgInativo.Location = new System.Drawing.Point(19, 121);
            this.imgInativo.Name = "imgInativo";
            this.imgInativo.Size = new System.Drawing.Size(100, 37);
            this.imgInativo.TabIndex = 43;
            this.imgInativo.TabStop = false;
            this.imgInativo.Visible = false;
            // 
            // lblID
            // 
            this.lblID.BackColor = System.Drawing.Color.Lavender;
            this.lblID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblID.Location = new System.Drawing.Point(56, 9);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(52, 20);
            this.lblID.TabIndex = 5;
            this.lblID.Text = "0";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(6, 11);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(43, 13);
            this.lblCodigo.TabIndex = 4;
            this.lblCodigo.Text = "C�digo:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(11, 41);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(38, 13);
            this.lblNome.TabIndex = 6;
            this.lblNome.Text = "Nome:";
            // 
            // chkAdmin
            // 
            this.chkAdmin.AutoSize = true;
            this.chkAdmin.Location = new System.Drawing.Point(56, 93);
            this.chkAdmin.Name = "chkAdmin";
            this.chkAdmin.Size = new System.Drawing.Size(89, 17);
            this.chkAdmin.TabIndex = 2;
            this.chkAdmin.Text = "Administrador";
            this.chkAdmin.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Senha:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(56, 37);
            this.txtNome.MaxLength = 80;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(304, 20);
            this.txtNome.TabIndex = 0;
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(56, 66);
            this.txtSenha.MaxLength = 12;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(304, 20);
            this.txtSenha.TabIndex = 1;
            // 
            // frmCadUsuarios
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(388, 176);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkAdmin);
            this.Controls.Add(this.imgInativo);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.cmdCanc);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.cmdOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadUsuarios";
            this.ShowIcon = false;
            this.Text = "Usu�rios";
            this.Load += new System.EventHandler(this.form_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.form_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.imgInativo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip T;
        private System.Windows.Forms.PictureBox imgInativo;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Button cmdCanc;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.CheckBox chkAdmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtSenha;
    }
}
