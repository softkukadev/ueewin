﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ueeWin
{
    class CUser
    {
        public int id { get; set; }
        public string login { get; set; }
        public string ssd { get; set;  }
        public bool isAdmin { get; set; }
        public bool ativo { get; set; }
        public string hashSsd { get; set; }

        public static DataSet getAll(string CritID, string ID, string CritLogin, string Login, string Estado)
        {
            //SQL Base da consulta
            string SQL = "SELECT  id, login, isAdmin, ativo FROM sysUsuario WHERE 1=1 ";
            if (Estado == "A") SQL += "AND Ativo = 1 ";
            if (Estado == "I") SQL += "AND Ativo = 0 ";

            //Filtra por código
            if (CritID != "all")
            {
                switch (CritID)
                {
                    case "=": SQL += " AND id = " + ID + " "; break;
                    case ">": SQL += " AND id > " + ID + " "; break;
                    case "<": SQL += " AND id < " + ID + " "; break;
                }
            }

            //Filtra por nome
            if (CritLogin != "all" && !string.IsNullOrEmpty(Login))
            {
                switch (CritLogin)
                {
                    case "=": SQL += " AND Login = '" + Login.ToSQL() + "' "; break;
                    case "ct": SQL += " AND Login like '%" + Login.ToSQL() + "%' "; break;
                    case "cc": SQL += " AND Login like '" + Login.ToSQL() + "%' "; break;
                }
            }

            //Executa a consulta
            SqlDataAdapter DA = new SqlDataAdapter(SQL, CConfig.CS);
            DataSet DS = new DataSet();
            DA.Fill(DS);
            return DS;

        }

        public static CUser getById(int id)
        {
            CUser ret = new CUser();
            ret.id = -1;

            string SQL = "SELECT  id, login, ssd, isAdmin, ativo FROM sysUsuario (nolock) WHERE id = " + id;
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();
            SqlCommand Cmd = new SqlCommand(SQL, cn);
            SqlDataReader DR = Cmd.ExecuteReader();
            if (DR.HasRows)
            {
                DR.Read();
                ret.id = int.Parse(DR["id"].ToString());
                ret.login = DR["login"].ToString().Trim();
                ret.isAdmin = (bool)DR["isAdmin"];
                ret.ativo = (bool)DR["ativo"];
                ret.hashSsd = DR["ssd"].ToString().Trim();
            }
            cn.Close();

            return ret;
        }

        public static int save(CUser xUser)
        {
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();
            SqlCommand Cmd = new SqlCommand("", cn);

            string sql = "";

            if (xUser.id < 0)
            {
                sql = "insert sysUsuario (login, ssd, isadmin, ativo) values (";
                sql += "'" + xUser.login.ToSQL() + "', ";
                sql += "'" +  CLogin.getMD5Hash(xUser.ssd.ToSQL()) + "', ";
                sql += xUser.isAdmin.ToSQL() + ",";
                sql += "1)";

                Cmd.CommandText = sql;
                int alt = Cmd.ExecuteNonQuery();
                if (alt != 1) throw new Exception("Erro ao inserir o registro.");

                sql = "select @@identity";
                Cmd.CommandText = sql;
                xUser.id = int.Parse(Cmd.ExecuteScalar().ToString());
            } else
            {
                sql = "update sysUsuario set ";
                sql += "login = '" + xUser.login.ToSQL() + "', ";
                sql += "isAdmin = " + xUser.isAdmin.ToSQL() + " ";
                sql += "where id = " + xUser.id;

                Cmd.CommandText = sql;
                int alt = Cmd.ExecuteNonQuery();
                if (alt != 1) throw new Exception("Erro ao alterar o registro.");
            }

            cn.Close();

            return xUser.id;
        }

        public static bool delete(int id)
        {
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();

            string sql = "delete sysUsuario where id = " + id;
            SqlCommand Cmd = new SqlCommand(sql, cn);

            int alt = Cmd.ExecuteNonQuery();
            if (alt != 1) throw new Exception("Erro ao exluir o registro.");

            cn.Close();
            return true;

        }

        public static bool activate(int id)
        {
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();

            string sql = "update sysUsuario set ativo = 1 where id = " + id;
            SqlCommand Cmd = new SqlCommand(sql, cn);

            int alt = Cmd.ExecuteNonQuery();
            if (alt != 1) throw new Exception("Erro ao ativar o registro.");

            cn.Close();
            return true;
        }

        public static bool deactivate(int id)
        {
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();

            string sql = "update sysUsuario set ativo = 0 where id = " + id;
            SqlCommand Cmd = new SqlCommand(sql, cn);

            int alt = Cmd.ExecuteNonQuery();
            if (alt != 1) throw new Exception("Erro ao ativar o registro.");

            cn.Close();
            return true;
        }

        public static bool changeSSD(int id, string newSSD)
        {

            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();

            string sql = "update sysUsuario set ssd = '"  + CLogin.getMD5Hash(newSSD.Trim().ToSQL()) + "' where id = " + id;
            SqlCommand Cmd = new SqlCommand(sql, cn);

            int alt = Cmd.ExecuteNonQuery();
            if (alt != 1) throw new Exception("Erro ao trocar a senha.");

            cn.Close();
            return true;
        }

    }
}
