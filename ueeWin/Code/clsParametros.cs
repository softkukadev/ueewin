﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ueeWin
{
    class CParam
    {
        public static string getParametro(string chave)
        {
            string ret = "";
            SqlConnection Cn = new SqlConnection(CConfig.CS);
            Cn.Open();
            SqlCommand Cmd = new SqlCommand("select valor from sysconfig where chave = '" + chave + "'", Cn);
            SqlDataReader DR = Cmd.ExecuteReader();
            DR.Read();
            if (DR.HasRows)
            {
                ret = DR["valor"].ToString();
            }

            DR.Close();
            Cn.Close();

            return ret;
        }

        public static bool setParametro(string chave, string valor)
        {
            SqlConnection Cn = new SqlConnection(CConfig.CS);
            Cn.Open();
            SqlCommand Cmd = new SqlCommand("update sysconfig set valor = '" + valor.ToSQL()  + "' where chave = '" + chave.ToSQL() + "'", Cn);
            int alt = Cmd.ExecuteNonQuery();
            Cn.Close();

            return alt == 1 ? true : false;
        }

    }
}
