﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ueeWin
{
    class CLogin
    {
        public static int idLogin = -1;
        public static string userLogin = "";
        public static bool isAdmin = false;
        public static bool ativo = false;

        public static string getMD5Hash(string valor)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(valor);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static bool login(string user, string pwd)
        {
            SqlConnection cn = new SqlConnection(CConfig.CS);
            cn.Open();

            string sql = "select count(*) from sysUsuario where login = '" + user.Trim() + "' and ssd = '" + getMD5Hash(pwd.Trim()) + "'" ;
            SqlCommand cmd = new SqlCommand(sql, cn);
            int qtd = int.Parse(cmd.ExecuteScalar().ToString());

            //Obtem dados do loging
            sql = "select id, isAdmin, ativo from sysUsuario where login = '" + user.Trim() + "' and ssd = '" + getMD5Hash(pwd.Trim()) + "'";
            cmd.CommandText = sql;
            SqlDataReader DR = cmd.ExecuteReader();
            if (DR.HasRows)
            {
                DR.Read();
                idLogin = int.Parse(DR["id"].ToString());
                userLogin = user;
                isAdmin = (bool)DR["isAdmin"];
                ativo = (bool)DR["ativo"];
            } else
            {
                idLogin = -1;
                userLogin = "";
                isAdmin = false;
                ativo = false;
            }

            DR.Close();
            cn.Close();

            return qtd > 0;
        }

    }
}
