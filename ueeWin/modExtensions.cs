﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ueeWin
{
    public static class Extensions
    {
        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToSQL(this decimal d)
        {
            string Valor = d.ToString();
            Valor = Valor.Replace(".", "");
            Valor = Valor.Replace(",", ".");

            if (Valor.Trim() == "")
                return "NULL";
            else
                return Valor;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToSQL(this DateTime d)
        {
            return d.ToString("MM/dd/yyyy");
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToSQL(this string s)
        {
            if (s == null)
                return "";
            else
                return s.Replace("'", "''").Trim();
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToSQL(this bool b)
        {
            if (b) return "1"; else return "0";
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static bool IsAlpha(this string strToCheck)
        {
            System.Text.RegularExpressions.Regex objAlphaPattern = new System.Text.RegularExpressions.Regex("[^a-zA-Z]");
            return !objAlphaPattern.IsMatch(strToCheck);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string RetiraAcentos(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return "";

            string C = "", r = "";

            for (int i = 0; i < s.Length; i++)
            {
                C = s.Substring(i, 1);
                switch (C)
                {
                    case "Á":
                    case "À":
                    case "Ã":
                    case "Â":
                    case "Ä":
                        C = "A"; break;
                    case "á":
                    case "à":
                    case "ã":
                    case "â":
                    case "ä":
                        C = "a"; break;
                    case "É":
                    case "È":
                    case "Ê":
                    case "Ë":
                        C = "E"; break;
                    case "é":
                    case "è":
                    case "ê":
                    case "ë":
                        C = "e"; break;
                    case "Ì":
                    case "Í":
                    case "Î":
                    case "Ï":
                        C = "I"; break;
                    case "í":
                    case "ì":
                    case "î":
                    case "ï":
                        C = "i"; break;
                    case "Ó":
                    case "Ò":
                    case "Õ":
                    case "Ô":
                    case "Ö":
                        C = "O"; break;
                    case "ó":
                    case "ò":
                    case "õ":
                    case "ô":
                    case "ö":
                        C = "o"; break;
                    case "Ú":
                    case "Ù":
                    case "Û":
                    case "Ü":
                        C = "U"; break;
                    case "ú":
                    case "ù":
                    case "û":
                    case "ü":
                        C = "u"; break;
                    case "Ç":
                        C = "C"; break;
                    case "ç":
                        C = "c"; break;
                    case "º":
                        C = " "; break;
                    case "ª":
                        C = " "; break;
                }
                r = r + C;
            }
            return r;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string NumeroNFe(this string valor)
        {
            string Ret;
            Ret = valor.Replace(".", "");
            Ret = Ret.Replace(",", ".");
            return Ret;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string PontoToVirg(this string valor)
        {
            string Ret;
            Ret = valor.Replace(".", ",");
            return Ret;
        }


        [System.Diagnostics.DebuggerStepThrough()]
        public static int ToInteiro(this string s)
        {
            if (String.IsNullOrEmpty(s)) s = "0";
            return int.Parse(s.Replace(".", ""));
        }


        [System.Diagnostics.DebuggerStepThrough()]
        public static long ToInteiroLongo(this string s)
        {
            if (String.IsNullOrEmpty(s)) s = "0";
            return long.Parse(s.Replace(".", ""));
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static int ToInteiro(this object o)
        {
            if (o == null) o = "0";
            if (o.ToString() == "") o = "0";
            return int.Parse(o.ToString());
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static decimal ToDec(this string s)
        {
            if (String.IsNullOrEmpty(s)) s = "0";
            s = s.Replace(".", "");
            return decimal.Parse(s);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToStringSemNulos(this object o)
        {
            if (o == null) o = "";
            if (o.ToString() == "") o = "";
            return o.ToString();
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static decimal ToDec(this object o)
        {
            if (o == null) o = "0";
            if (o.ToString() == "") o = "0";
            return decimal.Parse(o.ToString());
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static DateTime ToDataNF(this string data)
        {
            string formatacao = ""; DateTime d = DateTime.MinValue;
            if (data.Trim() != "")
            {
                foreach (string s in data.Split('-'))
                    formatacao = s + "/" + formatacao;
                formatacao = formatacao.Substring(0, formatacao.Length - 1);
                d = DateTime.Parse(formatacao);
            }
            return d;
        }


        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToRPT(this DateTime data)
        {
            string formatacao = "";
            formatacao = data.Year.ToString() + "," + data.Month.ToString() + "," + data.Day.ToString();
            return formatacao;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static DateTime ToDate(this object o)
        {
            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(o.ToString(), out dt))
            {
                dt = DateTime.Parse(o.ToString());
            }

            return dt;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToDateNULL(this object o)
        {
            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(o.ToString(), out dt))
            {
                dt = DateTime.Parse(o.ToString());
            }

            string s = "";
            if (dt > DateTime.MinValue) s = dt.ToString("dd/MM/yyyy");
            return s;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static bool IsDec(this object o)
        {
            decimal d = 0M;
            if (o == null) o = "0";
            if (o.ToString() == "") o = "0";
            return decimal.TryParse(o.ToString(), out d);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static bool IsDec(this string s)
        {
            decimal d = 0M;
            if (s == "") s = "0";

            return decimal.TryParse(s, out d);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static bool ToBool(this object o)
        {
            bool ok = false;
            if (o == null) o = false;
            if (bool.TryParse(o.ToString(), out ok))
            {
                if (o.ToString() == bool.TrueString) ok = true;
            }

            return ok;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static bool ToBool(this string s)
        {
            bool ok = false;
            if (s == "") ok = false;
            if (bool.TryParse(s, out ok))
            {
                if (s == bool.TrueString) ok = true;
            }

            return ok;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string NumeroArquivo(this decimal d)
        {
            string s = d.ToString("###,###0.00").Replace(".", "");
            s = s.ToString().Replace(",", "");

            return s;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string FormatoArquivo(this string s)
        {
            return s.RetiraAcentos().Trim().ToUpper();
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static decimal ToNumero(this string s)
        {
            string centavos = "";
            string valor = "";

            valor = s.Substring(0, s.Length - 2);
            centavos = s.Substring(s.Length - 2, 2);

            valor = valor + "," + centavos;

            return valor.ToDec();
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static DateTime ToDataBradesco(this string s)
        {
            DateTime DT;

            DT = new DateTime(s.Substring(0, 4).ToInteiro(), s.Substring(4, 2).ToInteiro(), s.Substring(6, 2).ToInteiro());

            return DT;

        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static DateTime ToDataTxt(this string s)
        {
            DateTime DT;

            DT = new DateTime(s.Substring(4).ToInteiro(), s.Substring(2, 2).ToInteiro(), s.Substring(0, 2).ToInteiro());

            return DT;

        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToExtenso(this DateTime dt)
        {
            string mes = "";
            switch (dt.Month)
            {
                case 1: mes = "janeiro"; break;
                case 2: mes = "fevereiro"; break;
                case 3: mes = "março"; break;
                case 4: mes = "abril"; break;
                case 5: mes = "maio"; break;
                case 6: mes = "junho"; break;
                case 7: mes = "julho"; break;
                case 8: mes = "agosto"; break;
                case 9: mes = "setembro"; break;
                case 10: mes = "outubro"; break;
                case 11: mes = "novembro"; break;
                case 12: mes = "dezembro"; break;
            }

            int dia = dt.Day;
            int ano = dt.Year;


            string Dia = new CExtenso((decimal)dia).ToString().Replace("reais", "dias").Replace("real", "dia").Replace("hum", "um");
            string Ano = new CExtenso((decimal)ano).ToString().Replace("reais", "");

            string data = Dia + " do mês de " + mes + " de " + Ano;

            return data;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToExtensoNumerico(this DateTime dt)
        {
            string mes = "";
            switch (dt.Month)
            {
                case 1: mes = "janeiro"; break;
                case 2: mes = "fevereiro"; break;
                case 3: mes = "março"; break;
                case 4: mes = "abril"; break;
                case 5: mes = "maio"; break;
                case 6: mes = "junho"; break;
                case 7: mes = "julho"; break;
                case 8: mes = "agosto"; break;
                case 9: mes = "setembro"; break;
                case 10: mes = "outubro"; break;
                case 11: mes = "novembro"; break;
                case 12: mes = "dezembro"; break;
            }

            int dia = dt.Day;
            int ano = dt.Year;


            string data = dia + " de " + mes + " de " + ano;

            return data;
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public static string ToUTC(this DateTime Data)
        {
            string s = "";
            if (Data != DateTime.MinValue)
            {
                s = Data.ToString("yyyy-MM-dd") + "T" + Data.ToString("HH:mm:ss");

                if (Data.IsDaylightSavingTime())
                    s += "-02:00";
                else
                    s += "-03:00";
            }
            return s;
        }

    }
}
