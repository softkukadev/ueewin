﻿namespace ueeWin
{
    partial class frmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.staMain = new System.Windows.Forms.StatusStrip();
            this.staUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuCad = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCadUser = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.staMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // staMain
            // 
            this.staMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staUser});
            this.staMain.Location = new System.Drawing.Point(0, 428);
            this.staMain.Name = "staMain";
            this.staMain.Size = new System.Drawing.Size(800, 22);
            this.staMain.TabIndex = 1;
            this.staMain.Text = "statusStrip1";
            // 
            // staUser
            // 
            this.staUser.Name = "staUser";
            this.staUser.Size = new System.Drawing.Size(62, 17);
            this.staUser.Text = "<<User>>";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCad,
            this.mnuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuCad
            // 
            this.mnuCad.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCadUser});
            this.mnuCad.Name = "mnuCad";
            this.mnuCad.Size = new System.Drawing.Size(71, 20);
            this.mnuCad.Text = "&Cadastros";
            // 
            // mnuCadUser
            // 
            this.mnuCadUser.Name = "mnuCadUser";
            this.mnuCadUser.Size = new System.Drawing.Size(119, 22);
            this.mnuCadUser.Text = "&Usuários";
            this.mnuCadUser.Click += new System.EventHandler(this.mnuCadUser_Click);
            // 
            // mnuHelp
            // 
            this.mnuHelp.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpConfig});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(24, 20);
            this.mnuHelp.Text = "?";
            // 
            // mnuHelpConfig
            // 
            this.mnuHelpConfig.Name = "mnuHelpConfig";
            this.mnuHelpConfig.Size = new System.Drawing.Size(180, 22);
            this.mnuHelpConfig.Text = "&Configuração";
            this.mnuHelpConfig.Click += new System.EventHandler(this.mnuHelpConfig_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.staMain);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.staMain.ResumeLayout(false);
            this.staMain.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip staMain;
        private System.Windows.Forms.ToolStripStatusLabel staUser;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuCad;
        private System.Windows.Forms.ToolStripMenuItem mnuCadUser;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpConfig;
    }
}

