﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmLogin : Form
    {
        public bool OK = false;

        public frmLogin()
        {
            InitializeComponent();
        }

        private void frmLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) SendKeys.Send("{TAB}");
        }

        private void cmdSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            //Validar
            if(txtLogin.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe o login do usuario");
                txtLogin.Focus();
                return;
            }

            if (txtSenha.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe a senha do usuario");
                txtSenha.Focus();
                return;
            }

            //Tentar logar
            bool resLogin = CLogin.login(txtLogin.Text, txtSenha.Text);

            if (!resLogin)
            {
                CGeral.MsgErro("Credenciais incorretas.");
                txtSenha.Focus();
            } else
            {
                if (!CLogin.ativo)
                {
                    CGeral.MsgErro("O login informado está inativo.");
                    txtLogin.Focus();
                } else {
                    this.OK = true;
                    this.Close();
                }
            }
        }


    }
}
