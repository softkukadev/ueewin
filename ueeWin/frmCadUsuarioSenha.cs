﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ueeWin
{
    public partial class frmCadUsuarioSenha : Form
    {
        public int ID;
        private string senhaAtual = "";

        public frmCadUsuarioSenha()
        {
            InitializeComponent();
        }

        private void frmCadUsuarioSenha_Load(object sender, EventArgs e)
        {
            CUser xUser = CUser.getById(this.ID);
            this.Text = "Alterando senha do usuário " + xUser.login;

            senhaAtual = xUser.hashSsd;
        }

        private void frmCadUsuarioSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return) SendKeys.Send("{TAB}");
        }

        private void cmdCanc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool DadosValidos()
        {

            if (txtSenhaNova.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe a nova senha.");
                txtSenhaNova.Focus();
                return false;
            }

            if (txtConf.Text.Trim() == "")
            {
                CGeral.MsgErro("Informe a confirmação da senha.");
                txtConf.Focus();
                return false;
            }

            if (txtSenhaNova.Text.Trim() != txtConf.Text.Trim())
            {
                CGeral.MsgErro("Confirmação incorreta.");
                txtConf.Focus();
                return false;
            }
            return true;
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            if (!DadosValidos())
                return;

            CUser.changeSSD(this.ID, txtSenhaNova.Text.Trim());

            CGeral.Msg("Senha alterada com sucesso");
            this.Close();

        }
    }
}
