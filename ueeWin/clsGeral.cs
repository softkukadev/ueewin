﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ueeWin
{
    class CGeral
    {
        public static void Msg(string mensagem)
        {
            System.Windows.Forms.MessageBox.Show(mensagem, CConfig.NOME_APP, System.Windows.Forms.MessageBoxButtons.OK);
        }

        public static void MsgErro(string mensagem)
        {
            System.Windows.Forms.MessageBox.Show(mensagem, CConfig.NOME_APP, System.Windows.Forms.MessageBoxButtons.OK);
        }

    }
}
